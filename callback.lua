-- This file is licensed under the terms of the BSD 2-clause license.
-- See LICENSE.txt for details.

local function stripall(t)
	t=minetest.strip_colors(t)
	t=minetest.get_translated_string("en",t)
	return t
end

local abcd=false
local old_csa=minetest.chat_send_all
function minetest.chat_send_all(txt,...)
	if not abcd then
		irc.say(stripall(txt))
	end
	return old_csa(txt,...)
end
function irc.chat_send_all(txt)
	abcd=true
	minetest.chat_send_all(txt)
	abcd=false
end

minetest.register_on_chat_message(function(name, message)
	if not irc.connected
	   or message:sub(1, 1) == "/"
	   or message:sub(1, 5) == "[off]"
	   or not irc.joined_players[name]
	   or (not minetest.check_player_privs(name, {shout=true})) then
		return
	end
	local nl = message:find("\n", 1, true)
	if nl then
		message = message:sub(1, nl - 1)
	end
	irc.say(irc.playerMessage(name, stripall(message)))
end)


minetest.register_on_shutdown(function()
	irc.disconnect("Game shutting down.")
end)

